import concurrent.futures

from bad_code import this_is_bad
from good_code import this_is_good
from ugly_code import this_is_ugly


def run_code_in_threads(how_many_things: int = 100000):
    values = [how_many_things, how_many_things, how_many_things]
    our_good_bad_ugly = [this_is_good, this_is_bad, this_is_ugly]

    with concurrent.futures.ThreadPoolExecutor(max_workers=5) as executor:
        our_futures = []
        for v, f in zip(values, our_good_bad_ugly):
            a_future = executor.submit(f, v)
            our_futures.append(a_future)
        for future in concurrent.futures.as_completed(our_futures):
            future.result()


if __name__ == "__main__":
    run_code_in_threads(1000000)