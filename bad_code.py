def this_is_bad(how_many_things: int):
    l = [x for x in range(how_many_things)]
    b = [x * 10 for x in l]
    mean_value = 0
    for x in b:
        divided_by_len = x / len(b)
        mean_value += divided_by_len
    print(f"This is the bad one. The mean value is {mean_value}")
