from bad_code import this_is_bad
from good_code import this_is_good
from ugly_code import this_is_ugly


def do_many_things(how_many_things: int = 10000):
    print(f"Hi, we are doing {how_many_things} things")
    this_is_good(how_many_things)
    this_is_bad(how_many_things)
    this_is_ugly(how_many_things)


if __name__ == "__main__":
    do_many_things(1000000)

