import numpy as np


def this_is_good(how_many_things: int):
    a = np.linspace(0, how_many_things - 1, how_many_things, dtype=np.int32)
    b = np.multiply(a, 10.)
    mean_value = np.mean(b)
    print(f"This is the good one. The mean value is {mean_value}.")
    
